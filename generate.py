import sys

import cv2


try:
    camera = sys.argv[1]
    print('Reading frame from file `%s`.' % camera)
except IndexError:
    camera = 0
    print('Reading frame from camera device.')

faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
video_capture = cv2.VideoCapture(camera)
alpha = cv2.imread("alpha.png")

# Capture frame-by-frame
ret, frame = video_capture.read()

if frame is None:
    print('No frame could be read!')
    sys.exit(1)

gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

faces = faceCascade.detectMultiScale(
    gray,
    scaleFactor=1.1,
    minNeighbors=5,
    minSize=(30, 30),
    flags=cv2.CASCADE_SCALE_IMAGE,
)

# Get the biggest face and cut it out of the frame
if len(faces):
    x, y, w, h = max(faces, key=lambda f: f[2] * f[3])
    face = frame[y:y + h, x:x + w]
else:
    face = frame

resized = cv2.resize(face, (150, 150))

masked = cv2.merge((
    resized[:, :, 0],
    resized[:, :, 1],
    resized[:, :, 2],
    alpha[:, :, 0],
))

# Display the resulting frame
cv2.imwrite('generated/face.png', masked)

# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()
