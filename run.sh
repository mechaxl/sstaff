#!/usr/bin/env bash

set -eux

python generate.py

ffmpeg -y -i abstract.mp4 -i generated/face.png \
    -filter_complex "[0:v][1:v] overlay=165:165" \
    -pix_fmt yuv420p -c:a copy \
    generated/output.mp4

ffplay -loop 0 generated/output.mp4